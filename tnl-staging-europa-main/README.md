# tnl-pruebas-europa

![image](https://user-images.githubusercontent.com/62071619/151419528-4617fe03-88d7-4843-9c0b-576e202d86b7.png)

La infraestructura se ha diseñado siguiendo las siguientes premisas acordadas con el equipo técnico y de desarrollo de Tradenolimits.
Todos los entornos deben poder desplegarse de manera automatizada mediante código Terraform y deployment Terraform Cloud.
El repositorio del código será GitHub administrado por Tradenolimits.
El modelo de pipeline elegido sigue el siguiente ejemplo:

![image](https://user-images.githubusercontent.com/62071619/151419644-1d91feb0-2994-4cc9-8085-689845ee0727.png)


La arquitectura elegida se basa en clúster de Kubernetes EKS desplegado en AWS.
Cada región dispondrá de su propio clúster de producción en arquitectura multicapa, front-end, api, back-end
Adicionalmente existirá un único clúster de pre-producción
