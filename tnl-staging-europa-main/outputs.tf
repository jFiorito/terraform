
output "vpc_id" {
  description = "VPC ID."
  value       = module.vpc.vpc_id
}

output "private_subnets" {
  description = "private_subnets"
  value       = module.vpc.private_subnets
}

output "public_subnets" {
  description = "public_subnets"
  value       = module.vpc.public_subnets
}

output "cluster_id" {
  description = "EKS cluster ID."
  value       = module.eks.cluster_id
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}
output "cluster_oidc_url" {
  description = "OpenID Connect provider URL"
  value       = module.eks.cluster_oidc_issuer_url
}
/*
output "cluster_security_group_id" {
  description = "Security group ids attached to the cluster control plane."
  value       = module.eks.cluster_security_group_id
}

output "kubectl_config" {
  description = "kubectl config generado por el modulo."
  value       = module.eks.kubeconfig
}

output "config_map_aws_auth" {
  description = "yaml configuration tpara autenticarte al EKS cluster."
  value       = module.eks.config_map_aws_auth
}
*/
output "region" {
  description = "Región donde desplegamos el cluster"
  value       = var.aws_region
}

output "cluster_name" {
  description = "Nombre del Cluster"
  value       = local.cluster_name
}

output "comandos_alb_ingress_1" {
  description = "Comandos a ejecutar posteriores"
  value       = "aws eks --region ${var.aws_region} update-kubeconfig --name tnl-eks-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
}
output "comandos_alb_ingress_2" {
  description = "Comandos a ejecutar posteriores"
  value       = "eksctl utils associate-iam-oidc-provider --region ${var.aws_region} --cluster tnl-eks-${var.entorno}-${var.cluster_capa}-${var.cluster_region} --approve"
}
output "comandos_alb_ingress_3" {
  description = "Comandos a ejecutar posteriores"
  value       = "curl -o iam_policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.2.0/docs/install/iam_policy.json"
}
output "comandos_alb_ingress_4" {
  description = "Comandos a ejecutar posteriores"
  value       = "aws iam create-policy --policy-name tnl-${var.entorno}-${var.cluster_capa}-AWSLoadBalancerControllerIAMPolicy --policy-document file://iam_policy.json"
}
output "comandos_alb_ingress_5" {
  description = "Comandos a ejecutar posteriores"
  value       = "eksctl create iamserviceaccount --cluster=tnl-eks-${var.entorno}-${var.cluster_capa}-${var.cluster_region} --namespace=kube-system --name=aws-load-balancer-controller --attach-policy-arn=arn:aws:iam::099708645471:policy/tnl-${var.entorno}-${var.cluster_capa}-AWSLoadBalancerControllerIAMPolicy --override-existing-serviceaccounts --approve"
}
output "comandos_alb_ingress_6" {
  description = "Comandos a ejecutar posteriores"
  value       = "kubectl apply -k github.com/aws/eks-charts/stable/aws-load-balancer-controller//crds?ref=master"
}
output "comandos_alb_ingress_7" {
  description = "Comandos a ejecutar posteriores"
  value       = "helm repo add eks https://aws.github.io/eks-charts"
}
output "comandos_alb_ingress_8" {
  description = "Comandos a ejecutar posteriores"
  value       = "helm upgrade -i aws-load-balancer-controller eks/aws-load-balancer-controller --set clusterName=tnl-eks-${var.entorno}-${var.cluster_capa}-${var.cluster_region} --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller -n kube-system"
}
output "comandos_alb_ingress_9" {
  description = "Comandos a ejecutar posteriores"
  value       = "kubectl get deployment -n kube-system aws-load-balancer-controller"
}


