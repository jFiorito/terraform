
resource "aws_lb_target_group" "tf_eks" {
  name = "tnl-tg-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
  port = 80
  protocol = "HTTP"
  vpc_id = module.vpc.vpc_id
  target_type = "ip"
  depends_on = [module.eks]
    tags = {
    "ingress.k8s.aws/resource"  = "tnl-app-data"
    "ingress.k8s.aws/stack"     = "tnl-app-data"
    "elbv2.k8s.aws/cluster"     = "tnl-eks-tes-front-europa"
  }
}

resource "aws_security_group" "eks-alb" {
  name        = "tnl-sg-public-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
  description = "Grupo de seguridad para permitir trafico publico"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "owned"
  }
  depends_on = [module.eks]
}

resource "aws_security_group_rule" "eks-alb-public-https" {
  description       = "Permite al balanceador comunicarse con trafico publico"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eks-alb.id}"
  to_port           = 443
  type              = "ingress"
  depends_on = [module.eks]
}
resource "aws_security_group_rule" "eks-alb-public-http" {
  description       = "Permite al balanceador comunicarse con trafico publico"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eks-alb.id}"
  to_port           = 80
  type              = "ingress"
  depends_on = [module.eks]
}
resource "aws_alb" "eks-alb" {
  name            = "tnl-alb-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
  subnets         = module.vpc.private_subnets
  security_groups = ["${module.eks.cluster_security_group_id}", "${aws_security_group.eks-alb.id}"]
  ip_address_type = "ipv4"
  depends_on = [module.eks]
   tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "owned"
  }
}

resource "aws_alb_listener" "eks-alb" {
  load_balancer_arn = "${aws_alb.eks-alb.arn}"
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.tf_eks.arn}"
  }
  
  depends_on = [module.eks]
}

data "aws_acm_certificate" "tradenolimits" {
  domain   = "app.tradenolimits.com"
  statuses = ["ISSUED"]
}

resource "aws_alb_listener" "eks-alb-ssl" {
  load_balancer_arn = "${aws_alb.eks-alb.arn}"
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${data.aws_acm_certificate.tradenolimits.arn}"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.tf_eks.arn}"
  }
  depends_on = [module.eks]
  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "owned"
  }
}
