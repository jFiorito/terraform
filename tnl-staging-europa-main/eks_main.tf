########################################################################################################################################
## Modulo para crear el cluster EKS unicamente con los workers y los permisos para acceder
##
## eks_main.tf
##   - cluster eks
##   - nodos workers
##   - permisos usuarios cluster



module "eks" {
  source                = "terraform-aws-modules/eks/aws"
  version               = "17.10.0"
  cluster_name          = local.cluster_name
  cluster_version       = var.cluster_version
  subnets               = module.vpc.private_subnets
  cluster_iam_role_name = "tnl-rol-cluster-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
  workers_role_name     = "tnl-rol-nodes-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"

  
  tags = {
    Terraform    = "TradeNoLimits-Infra-Terraform"
    Environment  = var.cluster_region
    project_name = var.project_name
  }

  vpc_id = module.vpc.vpc_id

  node_groups_defaults = {
    ami_type  = "AL2_x86_64"
    disk_size = 20
}


  node_groups = {
    eks_nodes = {
      name             = "nodes-${var.cluster_capa}"
      desired_capacity = 3
      max_capacity     = 6
      min_capaicty     = 1
      instance_types   = ["t3.medium"]
    }
  }

  map_users             = [
    {
      userarn = "arn:aws:iam::099708645471:user/arnaldo"
      username = "arnaldo"
      groups  = [
        "system:masters"
      ]
    },
    {
      userarn = "arn:aws:iam::099708645471:user/julian.castro"
      username = "julian.castro"
      groups = [
        "system:masters"
      ]
    },
    {
      userarn = "arn:aws:iam::099708645471:user/ga-actions-tnl"
      username = "ga-actions-tnl"
      groups = [
        "system:masters"
      ]
    }
  ]

  depends_on = [module.vpc]
  manage_aws_auth = true
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}


