########################################################################################################################################
## Bloque principal del código terrafor formado por los siguientes archivos
##
## main.tf X bloque de código principal que genera los siguientes recursos
##   - vpc con sub redes
##   - nombre del cluster

## eks_main.tf
##   - cluster eks
##   - nodos workers
##   - permisos usuarios cluster

## alb_main.tf
##   - target groups
##   - security groups
##   - balanceador de aplicaciones (alb)
##   - se asigna el certificado
##   - se crean los listener

## providers.tf
##   - proveedores que utilizamos

## variables.tf
##   - conjunto de variables del despliegue IMPORTANTE!!! Unico fichero a modificar para desplegar el cluster entre entornos y regiones

## versions.tf
##   - versiones de recursos y modulos utilizados

## outputs.tf
##   - detalle de lo desplegado 


data "aws_availability_zones" "available" {}

## formamos el nombre del cluster con las variables
locals {
  cluster_name = "tnl-eks-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
}


## unico modulo que crea toda la configuracion VPC del cluister incluyendo red y subred
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = ">=3.2.0"
  name                 = "tnl-vpc-${var.entorno}-${var.cluster_capa}-${var.cluster_region}"
  cidr                 = var.vnet_cidr
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = var.snet_private
  public_subnets       = var.snet_public
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}
